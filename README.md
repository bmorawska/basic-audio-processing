# Obliczenia ewolucyjne

Przetwarzanie obrazu i dźwięku - zadanie 3

**Autorzy:**

- Barbara Morawska 234096

- Andrzej Sasinowski 234118

Wydział‚ Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka

2019/2020

## Wymagania

- Python > 3.6

- Numpy (1.18.2)

- Matplotlib (3.2.0)

- Librosa (0.7.2)

## Zaimplementowane rozwiązanie

Program pozwala na znalezienie częstotliwości o najwyższych amplitudach w tym w szczególności skupia się na odnalezieniu częstotliwości
podstawowej wczyanego dźwięku.

Zastosowano w tym celu metody:

- autokorelacji

- analizy widma Fouriera

## Uruchomienie programu (użytkownik)

Aby uruchomic program należy w wejść do katalogu, w którym znajduje
się plik: `main.py` 

, a następnie wpisać komendę:

_Na systemie Windows_

```python
python main.py
```

_Na systemach Linux_

```python
python3 main.py
```
Po wykonaniu programu w powstałym kaatalogu ``.\tones`` pojawią się wygenerowane dźwięki.

## Zmiana ustawień programu

Program umożliwia zmianę pliku źródłowego `filename`, częstotliwości próbkowania `bitrate` w hercach, rozmiaru okna `winlength` i rozmiaru kroku `hops`.

Przykładowo:

```Python
filename = 'audiofiles/natural/viola/698Hz.wav'
bitrate = 16000  # Hz
hops = 512
winlength = 2048
```



#### Aktualizacja pliku requirements.txt

Po dodaniu biblioteki należy dodać ją też do pliku requirements.txt, w taki sposób:

```bash
pip freeze > requirements.txt
```
