import os
import pandas as pd

headings = [
    'fname',
    'path',
    'frequency',
]

db = []

inputDir: str = './audiofiles'
outputFile: str = 'audio_db.csv'

for root, directories, filenames in os.walk(inputDir):
    for filename in filenames:
        fname = filename.split('/')[-1]
        if 'Hz' in fname:
            freq = int(fname.split('Hz')[0])
        else:
            freq = 0
        db.append([fname, os.path.join(root, filename), freq])

df = pd.DataFrame(db, columns=headings)
df.to_csv(outputFile, index=False)
