import numpy as np
import matplotlib.pyplot as plt


def autocorrelation(signal: np.array, rate: int, frame_length: int = 2048, hop_length: int = 512):
    freqs = []
    frames = []
    for frame in range(0, len(signal) - hop_length, hop_length):
        sample = signal[frame: frame + frame_length]
        freq = autocorrelation_sample(sample, rate, frame_length)
        freqs.append(freq)
        frames.append(frame)

    return freqs, frames


def autocorrelation_sample(signal: np.array, rate: int, frame_length: int):
    acorr_values = np.zeros(len(signal))
    for m in range(len(signal)):
        auto_signal = np.roll(signal, m)
        corr = auto_signal * signal
        acorr_values[m] = np.sum(corr)

    sums = np.zeros(frame_length // 2)
    for s in range(1, frame_length // 2):
        sums[s] = np.sum(acorr_values[1::s])

    fund_freq = 0.0
    sensivity = 0.9
    max_val_idx = np.argmax(sums)
    max_val = sums[max_val_idx]

    while fund_freq == 0:
        for idx in reversed(range(max_val_idx + 1, len(sums))):
            if sums[idx] > max_val * sensivity:
                fund_freq = idx
                break
        sensivity *= 0.9
        if sensivity < 0.5:
            fund_freq = max_val_idx
            break

    freq = 1 / (fund_freq / rate)

    #plt.plot(sums)
    #plt.show()
    return freq


def fourier(signal: np.array, rate: int, frame_length: int = 2048, hop_length: int = 128):
    freqs = []
    frames = []

    for frame in range(0, len(signal) - hop_length, hop_length):
        sample = np.copy(signal[frame: frame + frame_length])

        hamming = np.hamming(len(sample))
        sample *= hamming

        freq = fourier_sample(sample, rate, frame_length)
        freqs.append(freq)
        frames.append(frame)

    return freqs, frames


def fourier_sample(signal: np.array, rate: int, frame_length: int):

    fu = np.abs(fft(signal))
    # fu = np.abs(dft(signal))

    fund_freq = 0.0
    sensivity = 0.9
    max_val_idx = np.argmax(fu)
    max_val = fu[max_val_idx]

    while fund_freq == 0:
        for idx in reversed(range(0, int(max_val_idx))):
            if fu[idx] > max_val * sensivity:
                fund_freq = idx
                break
        sensivity *= 0.9
        if sensivity < 0.75:
            fund_freq = max_val_idx
            break

    freq = rate / frame_length * fund_freq

    # plt.plot(fu[0: len(fu) // 2])
    # plt.show()

    return freq


def dft(fx):
    fx = np.asarray(fx, dtype=complex)
    M = fx.shape[0]
    fu = fx.copy()

    for i in range(M):
        u = i
        sum = 0
        for j in range(M):
            x = j
            tmp = fx[x] * np.exp(-2j * np.pi * x * u * np.divide(1, M, dtype=complex))
            sum += tmp
        # print(sum)
        fu[u] = sum
    # print(fu)

    return fu


def idft(fu):
    fu = np.asarray(fu, dtype=complex)
    M = fu.shape[0]
    fx = np.zeros(M, dtype=complex)

    for i in range(M):
        x = i
        sum = 0
        for j in range(M):
            u = j
            tmp = fu[u] * np.exp(2j * np.pi * x * u * np.divide(1, M, dtype=complex))
            sum += tmp
        fx[x] = np.divide(sum, M, dtype=complex)

    return fx


def fft(x):
    """
    Oblicza FFT dla sygnału x w 1D
    :param x:  sygnał jednowymiarowy
    :return: transformata furiera sygnału
    """
    n = len(x)

    if n == 1:
        return x

    left_side = x[::2]
    right_side = x[1::2]

    transformed_left = fft(left_side)
    transformed_right = fft(right_side)

    inverted_root = np.exp(2 * np.pi * 1j / n)
    root = 1

    res = np.zeros(n, dtype=np.complex)
    for index in range(0, int(n / 2)):
        res[index] = transformed_left[index] + root * transformed_right[index]
        res[int(index + n / 2)] = transformed_left[index] - root * transformed_right[index]
        root = root * inverted_root

    return res


def ifft(x):
    """
    Oblicza odwrotną, szybką transformatę Furiera 1D
    :param x: wynik transformaty Furiera 1D
    :return: odwrotną transforamtę Furiera 1D
    """
    x = np.asarray(x, dtype=complex)
    x_conjugate = np.conjugate(x)

    fx = fft(x_conjugate)

    fx = np.conjugate(fx)
    fx = fx / x.shape[0]

    return fx


def preEmphasis(signal: list, alpha: float) -> np.array:
    emphasized_signal = np.append(signal[0], signal[1:] - alpha * signal[:-1])
    return emphasized_signal
