import os
import librosa.display
from termcolor import colored

from processing import *


def tone_generator(freqs, counts, hops, bitrate):
    tones = []

    for i, f in enumerate(freqs):
        time = counts[i] * hops / bitrate
        tone = librosa.tone(f, duration=time, sr=bitrate)
        tones.append(tone)

    melody = np.hstack(tones)
    return melody


def summary_writer(filename, unique_freqs, indices, counts, hops):
    text = 'Plik ' + colored(filename, 'green', attrs=['bold']) + ' zawiera częstotliwości: \n'
    for i, f in enumerate(unique_freqs):
        text += '    - ' + colored("{:.2f} Hz".format(round(f, 2)), 'yellow')
        text += '  [od {:.2f} s do {:.2f} s)'.format(round(indices[i] * hops / bitrate, 2),
                                                     round((indices[i] * hops + counts[i] * hops) / bitrate, 2))
        text += ' czas trwania: ' + colored('{:.2f}s\n'.format(round(counts[i] * hops / bitrate, 2)), 'magenta')

    print(text)


def freqs_to_filename(unique_freqs, method: str):
    text = 'tone_' + method + '_'
    for i, f in enumerate(unique_freqs):
        text += '_' + str(int(f)) + 'Hz'
        if i > 5:
            text += '_itd'
            break
    text += '.wav'
    return text


def draw_plots(signal, frames, freqs, bitrate, method):
    time_signal = np.arange(0, len(signal)) / bitrate
    frames = np.array(frames) / bitrate

    if method == 'acorr':
        method = 'autokorelacja'
    elif method == 'fft':
        method = 'FFT'

    plt.subplot(2, 1, 1)
    plt.plot(time_signal, signal, 'r')
    plt.title('Sygnał oryginalny')
    plt.xlabel('Czas (s)')
    plt.ylabel('Amplituda')

    plt.subplot(2, 1, 2)
    plt.plot(frames, freqs, 'b')
    plt.title('Wykryte częstotliwości w czasie - metoda: ' + method)
    plt.xlabel('Czas (s)')
    plt.ylabel('Częstotliwość [Hz]')

    plt.show()


def correlation(filename, bitrate, hops, winlength, draw=False):
    signal, rate = librosa.load(filename, sr=bitrate, duration=1)

    freqs, frames = autocorrelation(signal, rate, hop_length=hops, frame_length=winlength)
    unique_freqs, indices, counts = np.unique(freqs, return_index=True, return_counts=True)

    summary_writer(filename, unique_freqs, indices, counts, hops)

    melody = tone_generator(unique_freqs, counts, hops, bitrate)
    librosa.output.write_wav(os.path.join('./tones', freqs_to_filename(unique_freqs, method='acorr')), melody, bitrate)

    if draw:
        draw_plots(signal, frames, freqs, bitrate, method='acorr')


def fft_analysis(filename, bitrate, hops, winlength, draw=False):

    signal, rate = librosa.load(filename, sr=bitrate, duration=1)
    preEmphasis(signal, alpha=0.0005)

    freqs, frames = fourier(signal, rate, frame_length=winlength, hop_length=hops)

    unique_freqs, indices, counts = np.unique(freqs, return_index=True, return_counts=True)
    melody = tone_generator(unique_freqs, counts, hops, bitrate)
    librosa.output.write_wav(os.path.join('./tones', freqs_to_filename(unique_freqs, method='fft')), melody, bitrate)

    summary_writer(filename, unique_freqs, indices, counts, hops)
    if draw:
        draw_plots(signal, frames, freqs, bitrate, method='fft')


directory = './tones'
if not os.path.exists(directory):
    os.makedirs(directory)

filename = 'audiofiles/natural/flute/443Hz.wav'
bitrate = 16000  # Hz
hops = 512
winlength = 2048

print(colored('\nAutokorelacja', 'red', attrs=['bold']))
correlation(filename=filename, bitrate=bitrate, hops=hops, winlength=winlength, draw=True)
print(colored('\nAnaliza widma Fouriera', 'red', attrs=['bold']))
fft_analysis(filename=filename, bitrate=bitrate, hops=hops, winlength=winlength, draw=True)
